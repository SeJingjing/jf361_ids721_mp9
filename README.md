# jf361_ids721_mp9

 ## Project Introduction
This project involves implememting Streamlit App with a Hugging Face Model.

## Project Requirments
- Create a website using Streamlit
- Connect to an open source LLM (Hugging Face)
- Deploy model via Streamlit or other service (accessible via browser)


## Project Setup
#### 1. Set up environment:
Install the necessary libraries 
```angular2html
pip install streamlit transformers
```

#### 2. Create a Python script for Streamlit app
Implement functions in `.py`file

#### 3. Run Streamlit app:
Use the following command to run the app
```angular2html
streamlit run app.py
```

#### 4. Deploy to Streamlit
Create a new account in `Streamlit` and link to GitHub.

In the main page, click `New app`, and then follow the instruction.
<p> 
   <img src="screenshot/str.png" /> 
</p>
This is the URL for my translation app: https://translatee2c.streamlit.app 

Click the URL above can reach such page.
<p> 
   <img src="screenshot/dep.png" /> 
</p>


## Project Description
This project implements the function that could translate English to Chinese.

## Screenshots
This is an example of success.
<p> 
   <img src="screenshot/success.png" /> 
</p>
This is an example of error.
<p> 
   <img src="screenshot/error.png" /> 
</p>